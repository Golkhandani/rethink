import * as r from "rethinkdb";
import * as http from "http";
import * as socket from "socket.io";
import * as express from "express";
import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';


const Database = "Tenant11";

class Staff {
    Id?: string;
    MediaId: string;
    FirstName: string;
    LastName: string;
    Email: string;
    Phones: string[];
    Password: string;
    IsEnabled: boolean;
    CreatedAt: Date;
    UpdatedAt: Date;

    static async Insert(data: Staff | Staff[], connection: r.Connection) {
        return await r.db(Database).table(Staff.name).insert(data).run(connection);
    }
}

class InvoiceItem {
    Id:string
    IngredientId:string;
    SupplierId:string;
    IsConfirmed: boolean;
    Quantity: number;

    constructor(Data:Partial<InvoiceItem>) {
        Object.assign(this, Data);
    }
}

class Invoice {
    Id?: string;
    ShortId?: string;
    ReasonId?: string;
    InventoryId: string;
    Type: InvoiceType;
    Status: InvoiceStatus;
    Description: string;
    ShippingPrice: number;
    Tax: number;

   
    OpenById?: string;
    OpenedAt?: Date;
    SentById?: string;
    SentAt?: Date;
    ConfirmedById?: string;
    ConfirmedAt?: Date;
    ReceivedById?: string;
    ReceivedAt?: Date
    CancelledById?: string;
    CancelledAt?: Date;

    Items: InvoiceItem[]



    IsEnabled: boolean;
    CreatedAt: Date;
    DeletedAt: Date;

    constructor(Data:Partial<Invoice>) {
        Object.assign(this, Data);
    }
}

enum InvoiceStatus {
    OPEN = "OPEN",
    SENT = "SENT",
    CONFIRMED = "CONFIRMED",
    RECEIVED = "RECEIVED",
    CANCELLED = "CANCELLED"
}

enum InvoiceType {
    BUY = "BUY",
    ISSUE = "ISSUE",
    TRANSFER = "TRANSFER",
    VARIANCE = 'VARIANCE'
}

enum MediaType {
    GENERAL = "GENERAL",
    PROFILE = "PROFILE"
}
class Media {
    Id?: string;
    Prefix: string;
    Suffix: string;
    Type: MediaType;
    CreatedAt: Date;
    DeletedAt: Date;
}

enum BranchType {
    OUTLET = "OUTLET",
    KITCHEN = "KITCHEN",
    INVENTORY = "INVENTORY",
}
class Location {
    coordinates: [number, number]
}
class Branch {
    Id?: string;
    MediaId: string;
    InventoryId: string;
    KitchenId: string;
    Name: string;
    Type: BranchType;
    GeoLocation: Location | r.Point;
    Address: string;
    ZipCode: string;
    Phones: string[]
    IsEnabled: boolean;
    CreatedAt: Date;
    DeletedAt: Date;

    constructor(Data: Partial<Branch>) {
        Object.assign(this, Data);
    }
    static async Insert(data: Branch | Branch[], connection: r.Connection) {
        return await r.db(Database).table(Branch.name).insert(data).run(connection);
    }

}


class Unit {
    Id: string;
    Name: string;
    Type: string;

    constructor(Data: Partial<Unit>) {
        Object.assign(this, Data);
    }
}

class IngredientType {
    Id?: string
    MediaId: string
    Genus: string
    Name: string
    constructor(Data: Partial<IngredientType>) {
        Object.assign(this, Data);
    }
}

class IngredientCategory {
    Id?: string;
    MediaId: string;
    Name: string;
    IsEnabled: boolean;
    CreatedAt: Date;
    DeletedAt: Date;
    constructor(Data: Partial<IngredientCategory>) {
        Object.assign(this, Data)
    }
}

class IngredientItem {
    Id?: string;
    TypeId: string;
    CategoryId: string;
    StockId: string;
    UnitId: string;
    Name: string;
    Capacity: number
    Waste: number;
    Used: number;
    Price: number;
    MinCapacityAlert: number;
    IsEnabled: boolean
    CreatedAt: Date;
    DeletedAt: Date;

    constructor(Data: Partial<IngredientItem>) {
        Object.assign(this, Data)
    }
}


class Supplier {
    Id?: string;
    CityId: string;
    Name: string;
    Email: string;
    Phones: string;
    Address: string;
    ZipCode: string;
    GeoLocation: Location | r.Point;
    IsEnabled: boolean;
    CreatedAt: Date;
    DeletedAt: Date;

    static async Insert(data: Supplier | Supplier[], connection: r.Connection) {
        return await r.db(Database).table(Supplier.name).insert(data).run(connection);
    }
}

// const Table = 'Authors';
// const Server = http.createServer();
// const IO = socket(Server);


const App = express();

(async () => {
    const connection = await r.connect({ host: 'localhost', port: 28015, db: Database })


    r.dbCreate(Database).run(connection).catch(error => console.log("Already Created"))
    r.db(Database).tableCreate(Staff.name       , { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));
    r.db(Database).tableCreate(Branch.name      , { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));
    r.db(Database).tableCreate(Supplier.name    , { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));

    r.db(Database).tableCreate(Unit.name, { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));


    r.db(Database).tableCreate(IngredientType.name, { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));
    r.db(Database).tableCreate(IngredientCategory.name, { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));
    r.db(Database).tableCreate(IngredientItem.name, { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));

    r.db(Database).tableCreate(Invoice.name, { primary_key: "Id" }).run(connection).catch(error => console.log("Already Created"));




    App.route("/Ping").get((req, res) => res.send("Pong"));

    App.route("/Branch")
        .get(async (req, res) => {
            const Data =
                await r.table(Branch.name)
                    .map(Doc => {
                        return r.expr({
                            InventoryId: "",
                            KitchenId: ""
                        }).merge(Doc);
                    })
                    .merge(Data => {
                        const Inventory =
                            r.table(Branch.name)
                                .get(Data("InventoryId"));
                        const Kitchen =
                            r.table(Branch.name)
                                .get(Data("KitchenId"));

                        return {
                            Inventory,
                            Kitchen
                        };
                    })
                    .without("KitchenId", "InventoryId")
                    .run(connection);
            return res.send(await Data.toArray())


        })
        .post(async (req, res) => {
            const InventoryId = uuidv4();
            const KitchenId = uuidv4();
            const OutletId = uuidv4();


            const Inventory = new Branch({
                Id: InventoryId,
                Name: "Coochini's Inventory",
                Type: BranchType.INVENTORY,
                GeoLocation: r.point(45, 35),
                Address: "Tehran",
                ZipCode: "1234",
                Phones: ["09123243423"],
                IsEnabled: true,
                CreatedAt: new Date(),

            });

            const Kitchen = new Branch({
                Id: KitchenId,
                Name: "Coochini's Kitchen",
                Type: BranchType.KITCHEN,
                GeoLocation: r.point(45, 35),
                Address: "Tehran",
                ZipCode: "1234",
                Phones: ["09123243423"],
                IsEnabled: true,
                CreatedAt: new Date(),

            });

            const Outlet = new Branch({
                Id: OutletId,
                InventoryId: InventoryId,
                KitchenId: KitchenId,
                Name: "Coochini's Outlet",
                Type: BranchType.OUTLET,
                GeoLocation: r.point(45, 35),
                Address: "Tehran",
                ZipCode: "1234",
                Phones: ["09123243423"],
                CreatedAt: new Date(),
            });


            return res.send(
                await Branch.Insert(
                    [
                        Outlet,
                        Kitchen,
                        Inventory
                    ],
                    connection
                )
            );



        })


    App.route("/Unit")
        .get(async (req, res) => {
            const Data = await r.table(Unit.name).run(connection);
            return res.send(await Data.toArray());
        })
        .post(async (req, res) => {
            const Items = [
                {Name:'kg',          Type: 'WEIGHT' },        
                {Name:'gr',          Type: 'WEIGHT' },           
                {Name:'cc',          Type: 'WEIGHT' },           
                {Name:'lt',          Type: 'WEIGHT' },           
                {Name:'Km',          Type: 'DISTANCE' },           
                {Name:'M',           Type: 'DISTANCE' },           
                {Name:'Number / Kg', Type: 'INGREDIENT' },  
                {Name:'Number / gr', Type: 'INGREDIENT' },  
                {Name:'Number / Lt', Type: 'INGREDIENT' },  
                {Name:'Number / cc', Type: 'INGREDIENT' },  
                {Name:'Box / Kg',    Type: 'INGREDIENT' },   
                {Name:'Box / gr',    Type: 'INGREDIENT' },    
                {Name:'Box / Lt',    Type: 'INGREDIENT' },    
                {Name:'Box / cc',    Type: 'INGREDIENT' },    
            ].map(Item => {
                return new Unit({
                    Id: uuidv4(),
                    Name: Item.Name,
                    Type: Item.Type
                })
            })


            const Inserted = await r.table(Unit.name).insert(Items).run(connection);

            return res.send(Inserted);
        })


    App.route("/Ingredients/Type")
        .get(async (req, res) => {
            const Data = await r.table(IngredientType.name).run(connection);
            return res.send(await Data.toArray());
        })
        .post(async (req, res) => {
            const Items = [
                { Genus: 'Dairies', Name: 'Milk' },
                { Genus: 'Dairies', Name: 'Cheese' },
                { Genus: 'Dairies', Name: 'Cream' },
                { Genus: 'Dairies', Name: 'Yoghurt' },
                { Genus: 'Dairies', Name: 'Dough' },
                { Genus: 'Dairies', Name: 'Curd' },
                { Genus: 'Dairies', Name: 'Butter' },
                { Genus: 'Dairies', Name: 'Butter' },
                { Genus: 'Meats', Name: 'Beef Meat' },
                { Genus: 'Meats', Name: 'Pig Meat' },
                { Genus: 'Meats', Name: 'Fish Meat' },
                { Genus: 'Meats', Name: 'Buffalo Meat' },
                { Genus: 'Meats', Name: 'Chicken Meat' },
                { Genus: 'Meats', Name: 'Octopus Meat' },
                { Genus: 'Drinks', Name: 'Whiskey' },
                { Genus: 'Drinks', Name: 'Vodka' },
                { Genus: 'Drinks', Name: 'Tequila' },
                { Genus: 'Drinks', Name: 'Wine' },
                { Genus: 'Drinks', Name: 'Aragh' },
            ].map(Item => {
                return new IngredientType({
                    Id: uuidv4(),
                    Genus: Item.Genus,
                    Name: Item.Name
                })
            })


            const Inserted = await r.table(IngredientType.name).insert(Items).run(connection);

            return res.send(Inserted);
        })

    App.route("/Ingredients/Category")
        .get(async (req, res) => {
            const Data = await r.table(IngredientCategory.name).run(connection);
            return res.send(await Data.toArray());
        })
        .post(async (req, res) => {
            const Items = [
                'Vegetables',
                'Fruits',
                'Meats',
                'Drinks',
                'Juices',
                'Breads',
                'Nuts',
                'Spices',
                'Protein',
                'Ice Cream and cakes',
                'Coffee',
            ]
            const Categories = Items.map(Item => {
                return new IngredientCategory({
                    Id: uuidv4(),
                    Name: Item,
                    IsEnabled: true,
                    CreatedAt: new Date()
                })
            })

            const Inserted = await r.table(IngredientCategory.name).insert(Categories).run(connection);

            return res.send(Inserted);
        })

    App.route("/Ingredients/Item")
        .get(async (req, res) => {
            const Data = await r.table(IngredientItem.name).run(connection)
            return res.send(await Data.toArray());
        })
        .post(async (req, res) => {
            const Items = [
                { 
                    Name: 'Potato box',   
                    CategoryId: "5e587be3-f605-45bc-9b6a-02f96a241ec0", 
                    TypeId: "7db3cceb-05c9-4fe6-ab55-cf6d930f56b7", 
                    StockId: "fb13e8b2-1948-403b-8db9-c564809cfb1b", 
                    UnitId: "10c52663-3400-4da7-b7fb-0f32a2ef0797"
                },
                { 
                    Name: 'Pasta',        
                    CategoryId: "43ef0bd8-8c94-4d4d-9d7d-00a30c827736", 
                    TypeId: "047beb2e-9925-4cce-9c9e-17cd14fe92a4", 
                    StockId: "fb13e8b2-1948-403b-8db9-c564809cfb1b", 
                    UnitId: "c87c275a-d84f-47f2-8dcc-1c76646de924"
                },
            ].map(Item => {
                return new IngredientItem({
                    Id: uuidv4(),
                    TypeId: Item.TypeId,
                    CategoryId: Item.CategoryId,
                    StockId: Item.StockId,
                    UnitId: Item.UnitId,
                    Name: Item.Name,
                    Capacity: 1000,
                    Waste: 10,
                    Price: 12,
                    MinCapacityAlert: 200,
                    IsEnabled: true,
                    CreatedAt: new Date()
                })
            })
            const Inserted = await r.table(IngredientItem.name).insert(Items).run(connection);

            return res.send(Inserted)

        })


        App.route("/Invoice")
        .get(async (req, res)=>{
            const Cursor = 
                await r.table(Invoice.name)
                .merge(Item=>{
                    // return Item("Items")
                    return {
                        Items:(Item("Items") as any).map(_InvoiceItem => {
                            return { 
                                Ingredient: r.table(IngredientItem.name).get(_InvoiceItem("IngredientId")),
                                SupplierId: r.table(Supplier.name).get(_InvoiceItem("SupplierId")),
                                IsConfirmed: _InvoiceItem("IsConfirmed"),
                                Quantity: _InvoiceItem("IsConfirmed"),
                            }
                        })
                    }
                })
                .merge(Item => {
                    return {
                        "InventoryId": r.table(Branch.name).get(Item("InventoryId")),
                        "OpenById": r.table(Staff.name).get(Item("OpenById")),
                    }
                })
                .run(connection)
            
            
            return res.send(await Cursor.toArray());
        })
        .post(async (req, res)=>{
            const Items = [
                { IngredientId: "34c604d0-db45-4e8c-80e9-47c29f9b535a" },
                { IngredientId: "c7722ec7-eecb-4f47-a0c2-80b1b6a8c0d8" }
            ].map(Item=>{
                return new InvoiceItem({
                    Id: uuidv4(),
                    IngredientId: Item.IngredientId,
                    SupplierId: "a93078b3-04cc-4089-9a66-c3bbe15abc83",
                    IsConfirmed: false,
                    Quantity: 10
                }) 
            })

            const NewInvoice = new Invoice({
                Id: uuidv4(),
                ShortId : uuidv4().split("-")[0],
                InventoryId: "fb13e8b2-1948-403b-8db9-c564809cfb1b",
                Type: InvoiceType.BUY,
                Status: InvoiceStatus.OPEN,
                ShippingPrice: 20,
                Tax: 12,
                OpenById: "415c3570-2a85-4d93-a1a3-9df9e7afeb69",
                OpenedAt: new Date(),
                Items: Items,
                IsEnabled: true,
                CreatedAt: new Date()
            })

            const Inserted = await r.table(Invoice.name).insert(NewInvoice).run(connection);

            return res.send(Inserted)
        })


    App
        .route("/Staff")
        .get((req, res) => {
            r
                .table(Staff.name)
                .run(connection, (error, cursor) => {
                    return cursor.toArray((error, result) => {
                        return res.send(result)
                    });
                });

        })
        .post(async (req, res) => {
            return res.send(
                await Staff.Insert(
                    CreateStaff(),
                    connection
                )
            );
        });

    App
        .route('/Supplier')

            .get((Req, Res) => {
                r
                    .table(Supplier.name)
                    .run(connection, async (error, cursor) => {
                        return await cursor.toArray((error, result) => {
                            return Res.send(result)
                        });
                    })
                
            })

            .post(async (Req, Res) => {
                return Res.send(
                    await Supplier.Insert(
                        CreateSupplier(),
                        connection
                    )
                )
            })


    App.listen(3000, () => console.log("Started At http://localhost:3000"));



})();

function CreateStaff() {
    const Staffs = [];

    for (let i = 0; i < 20; i++) {
        const NewStaff = new Staff();
        NewStaff.Id = uuidv4();
        NewStaff.FirstName = faker.name.firstName();
        NewStaff.LastName = faker.name.lastName();
        NewStaff.Email = faker.internet.email();
        NewStaff.Phones = [faker.phone.phoneNumber()];
        NewStaff.Password = 'root';
        NewStaff.IsEnabled = faker.random.boolean();
        NewStaff.CreatedAt = new Date();
        NewStaff.UpdatedAt = null;

        Staffs.push(NewStaff);
    }

    return Staffs;
}

function CreateSupplier() {
    const NewSupplier       = new Supplier();
    NewSupplier.Id          = uuidv4();
    NewSupplier.CityId      = uuidv4();
    NewSupplier.Name        = faker.name.findName();
    NewSupplier.Email       = faker.internet.email();
    NewSupplier.Phones      = faker.phone.phoneNumber();
    NewSupplier.Address     = faker.address.streetAddress();
    NewSupplier.ZipCode     = faker.address.zipCode();
    NewSupplier.GeoLocation = [ faker.address.latitude(), faker.address.longitude() ];
    NewSupplier.IsEnabled   = faker.random.boolean();
    NewSupplier.CreatedAt   = new Date();
    NewSupplier.DeletedAt   = null;
    
    return NewSupplier;
}